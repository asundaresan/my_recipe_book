.. _burmese_tea_leaf_salad:

Burmese tea leaf salad
----------------------

Ingredients
~~~~~~~~~~~

* 4 tbsp fermented tea leaves, :ref:`fermented_tea_leaves`

* 4 cups shredded green cabbage or lettuce

* 1 cup cherry tomatoes, sliced

* 1 cup cucumber, sliced and chopped

* 2 cups tofu, sauteed or shallow fried

* ¼ cup fried garlic 

* ½ cup coarsely chopped toasted peanuts

* ¼ cup fried yellow split peas 

* 1 tablespoon fried garlic oil 

* 1 tablespoon lime juice

* ½ cup coarsely chopped fresh cilantro

* ¼ teaspoon crushed red pepper (to taste)

* jalapeño or serrano chile (to taste)



Instructions
~~~~~~~~~~~~


Prepare Fried Garlic 
++++++++++++++++++++

Place a fine-mesh strainer over a heatproof bowl. Heat 1/3 cup canola oil in a small skillet over medium heat. Reduce heat to low and add 1/4 cup sliced garlic; cook, stirring frequently, until the garlic is golden brown, about 4 minutes. Pour the garlic and oil through the strainer. Transfer the garlic to a paper-towel-lined plate. Reserve the oil to use on salads. Store fried garlic airtight in a cool dark place for up to 1 month; refrigerate the oil for up to 2 months. 

Prepare Fried Yellow Split Peas
+++++++++++++++++++++++++++++++

Soak 1/3 cup yellow split peas in water for at least 4 hours or up to 12 hours. Drain and pat dry. Place a fine-mesh strainer over a heatproof bowl. Heat 3/4 cup canola oil in a small skillet over medium heat. Add the split peas and cook, stirring often, until they start to turn a deep mustard color, 4 to 6 minutes. Pour the split peas and oil through the strainer (discard the oil). Transfer the split peas to a paper-towel-lined plate. Store airtight at room temperature for up to 2 weeks. 


Prepare the salad 
+++++++++++++++++

Fry the garlic first in a small cast iron pan. You may use the same pan to fry the yellow split peas and use it to toast the crushed peanuts.  Mix together various ingredients with the fermented tea leaves. Add sauteed tofu to make it a meal!


Description
~~~~~~~~~~~

Burmese tea leaf salad is a quick, healthy meal (except for the fried yellow split peas which can be prepared before hand and the fried garlic which can take some time to fry). It is best to soak the yellow split peas the day before and to prepare the garlic and split yellow peas for two days. 

