.. _dum_aloo: 

.. index::
   Indian

Dum Aloo
--------

Ingredients
~~~~~~~~~~~

* ½ pound Baby potatoes 

Paste 
+++++
* 1 cup onions, chopped 
* ½" ginger, roughly chopped  
* 5 cloves garlic, roughly chopped 
* ½ tsp fennel seeds 
* 2 tbsp water
* ¼ cup yogurt

Tadka 
+++++
* 2 tbsp ghee 
* 1" cinnamon 
* 2 cloves 
* 2 green cardamoms (?)
* 1 bay leaf 
* ½ tsp kalonji
* ½ tsp turmeric powder 
* ½ tsp coriander powder
* ½ tsp red chilly powder 
* ½ tsp turmeric powder 




Instructions
~~~~~~~~~~~~

* Boil potatos and shallow fry the potatoes.

* Soak 10 cashews for 20 minutes in water. Grind with 1 cup onions, ginger, garlic and fennel seeds. Add curd and blend once.

* Heat ghee, add cinnamon, cloves, cardamom, bay leaf and kalonji.

* Add onion curd paste, cover and cook on low flame for 7-8 minutes. 

* Add turmeric, red chilly powder, coriander powder and garam masala powder and saute. 

* Add baby potatoes, cover and cook for 12 minutes. Top with coriander leaves. 

Description
~~~~~~~~~~~

Adapted from `How to make Dum Aloo Restaurant Style <https://www.vegrecipesofindia.com/kashmiri-dum-aloo/#h-how-to-make-dum-aloo-restaurant-style>`_.
