.. _pumpkin_pie:

.. index::
   Pie
   Pumpkin 

Pumpkin Pie
-----------

Ingredients
~~~~~~~~~~~

Pumpkin
+++++++

* 1 medium sugar pumpkin (about 3 pounds)
* Canola oil, for oiling pumpkin

Pie Crust
+++++++++

* 2 cups all-purpose flour
* ¼ teaspoon salt
* ⅔ cup (11 tbsp) cold unsalted butter, cut into pieces

Filling
+++++++

* 14 oz can sweetened condensed milk
* 1/2 cup whipping cream
* 2 tablespoons cornstarch
* 2 tablespoons molasses
* 2 tablespoons canola oil
* 1 tablespoon ground cinnamon
* 1 teaspoon ground ginger
* 3 large eggs

Instructions
~~~~~~~~~~~~

Pie crust 
+++++++++
* For the easy pie crust: While the pumpkin is cooking, make the crust. In a large bowl, combine the flour and salt. Add in the butter and work into the dough with a fork until the mixture is crumbly. Stir in just enough cold water (4 to 5 tablespoons) with a fork just until the flour is moistened. Divide the dough in half, shape each half into a ball and flatten slightly. Wrap one ball in plastic wrap and refrigerate for another use.

Pumpkin 
+++++++ 

* Preheat the oven to 375 F.
* Remove the stem from the pumpkin and scrape out the insides, discarding the seeds. Cut the pumpkin in half and lay the pieces cut-side down on a rimmed baking sheet lined with aluminum foil. Rub canola oil all over the skin and bake until fork-tender, about 1 hour. Let cool.
* Roll out the remaining dough ball on a lightly floured surface to a 12-inch round. Transfer to a 9-inch-diameter glass pie dish. Fold the overhangs under and crimp decoratively. Pierce the dough all over with a fork. Chill in the refrigerator for 15 minutes.
* Line the crust with foil, fill with dried beans or pie weights and bake until the sides are set, about 12 minutes. Remove the foil and beans. Reduce the oven temperature to 350 degrees F.
* For the filling: Scoop out the pulp from the roasted pumpkin and puree in a food processor until smooth (you should have about 4 cups). Add the condensed milk, cream, cornstarch, molasses, canola oil, cinnamon, ginger, salt and eggs and combine thoroughly.
* Pour the filling into the crust and bake until the filling is set in the center, about 1 hour. Transfer the pie to a rack and cool for 30 minutes. Serve at room temperature or chilled.


Description
~~~~~~~~~~~


Adapted from `Nancy Fuller, From Scratch Pumpkin Pie <https://www.foodnetwork.com/recipes/nancy-fuller/from-scratch-pumpkin-pie-2251073>`_.
