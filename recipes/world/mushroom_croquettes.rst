Leek and Mushroom Croquettes
----------------------------

Ingredients
~~~~~~~~~~~

Makes 12 croquettes.

* 5 tablespoons unsalted butter
* 2 leeks, white and tender green parts only, thinly sliced
* ¼ pound shiitake mushrooms, stemmed and caps thinly sliced
* Salt and freshly ground pepper
* 1 teaspoon chopped thyme
* 1 teaspoon chopped oregano
* 3 tablespoons all-purpose flour, plus more for coating
* 1 cup milk
* ½ cup shredded Gruyère
* ¼ cup freshly grated Parmigiano-Reggiano
* 2 large eggs beaten with 2 tablespoons of water
* 1½ cups panko (Japanese bread crumbs)
* Vegetable oil, for frying

Instructions
~~~~~~~~~~~~

* In a large skillet, melt 2 tablespoons of butter. 
  Add leeks, shitake mushroom, salt and pepper and sautee on high heat for 7 minutes.
  Add thyme and oregano after switching off. 

* Melt 3 tbsp of butter. Whisk 3 tbsp of flour and cook for 1 minute in high heat. 
  Add the mil and cook, whisking until very thick and bubbly. 
  Scrape in the vegetable mixture, gruyere and parmesan and combine. 

* Spoon mixture onto a plastic wrap, log and freeze for 2 hours. 

* Make patties, dip in the batter, coat with bread crumbs and freeze for 15 minutes. 

* In a large skillet, heat ½ inch of oil to 375°F. Add all of the croquettes and fry over high heat, turning once or twice, until they are golden and crisp, about 5 minutes. Drain on paper towels and serve hot. 

Description
~~~~~~~~~~~

Adapted from `Leek and mushroom croquettes <https://www.foodandwine.com/recipes/leek-and-mushroom-croquettes>`_.

To make puff pastry, place in pastry sheets and bake at 400°F for 20 minutes. 
