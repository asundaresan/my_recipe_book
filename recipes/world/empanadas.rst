Empanadas
---------

Ingredients
~~~~~~~~~~~

Approximately 8 servings.

* 4 cups all-purpose flour

* 1 tsp baking powder

* 1 tbsp salt

* ¾ cup (1½ stick) cold unsalted butter, cut into small pieces

* 1¼ cup cold water


Instructions
~~~~~~~~~~~~

* Pulse flour, baking powder and salt with butter. Add water and pulse. Knead well.

* Refrigerate for 30 minutes or overnight.


Description
~~~~~~~~~~~

Adapted from `Basic empanadas <https://www.marthastewart.com/337617/empanadas>`_.

