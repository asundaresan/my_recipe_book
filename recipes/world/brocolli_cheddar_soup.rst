Broccoli Cheddar Soup
---------------------

Ingredients
~~~~~~~~~~~
* 6 tablespoons unsalted butter
* 1 small onion, chopped
* ¼ cup all-purpose flour
* 2 cups milk (substitued for half-and-half)
* 3 cups low-sodium broth
* 2 bay leaves
* ¼ teaspoon freshly grated nutmeg
* Kosher salt and freshly ground pepper
* 4 cups broccoli florets (about 1 head)
* 1 large carrot, diced
* 2½ cups (about 8 ounces) grated sharp cheddar cheese, plus more for garnish



Instructions
~~~~~~~~~~~~

* Melt the butter in a Dutch oven over medium heat. Add the onion and cook for 5 minutes. Whisk in the flour and cook until golden, 3 to 4 minutes. Gradually whisk in the milk, broth, bay leaves and nutmeg, then season with salt and pepper and bring to a simmer on medium-low for about 20 minutes.

* Add the broccoli and carrot to the broth mixture and simmer until tender, about 20 minutes. Discard the bay leaves. Puree the soup in batches in a blender until smooth; you'll still have flecks of carrot and broccoli. Return to the pot. 

* Add the cheese to the soup and whisk over medium heat until melted. Add up to ¾ cup water if the soup is too thick. 

Description
~~~~~~~~~~~

Adapted from `Almost-Famous Broccoli-Cheddar Soup <https://www.foodnetwork.com/recipes/food-network-kitchen/almost-famous-broccoli-cheddar-soup-recipe-1972744>`_
