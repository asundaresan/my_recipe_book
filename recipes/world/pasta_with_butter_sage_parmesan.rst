.. _pasta_butter_sage_parmesan:

.. index::
   pasta

Butter pasta with sage and parmesan 
-----------------------------------

Ingredients
~~~~~~~~~~~
Yield: 4 servings

* Salt and freshly ground black pepper
* 1 pound cut pasta, like ziti
* 2 tablespoons butter
* 30 fresh sage leaves
* 1 cup or more freshly grated Parmigiano-Reggiano
* Sliced onions or shallots (optional)
* Pine nuts (?)


Instructions
~~~~~~~~~~~~

* Bring a large pot of water to a boil and add salt and pasta. Cook until al dente. Keep a cup of water.

* Place butter in a large skillet or saucepan, add sage and cook on medium until sage shrivels. Optionally add sliced onions or shallots and nuts. 

* Drain and add the pasta to the butter-sage mixture, and raise heat to medium. Add ¾ cup of the water, and stir for 1 minute.

* Stir in cheese until the sauce becomes creamy. 

* Season with salt and pepper. 

Description
~~~~~~~~~~~

Adapted from `Pasta with butter, sage and parmesan <https://cooking.nytimes.com/recipes/5704-pasta-with-butter-sage-and-parmesan>`_
