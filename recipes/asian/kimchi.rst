.. _kimchi_:

.. index::
   Fermentation

Kimchi
----------------

Ingredients
~~~~~~~~~~~

* 3 pounds napa cabbage 
* 6 tbsp salt (to be washed away)
* 6 oz radish 
* 6 green onions 
* Asian chives (3 oz, a small bunch)
* 1 carrot 

To make the base kimchi paste

* 2 tbsp (glutinous, if available) rice flour
* 1 cup vegetable stock 
* 1 tbsp sugar 
* 2 tbsp salt

For the kimchi paste. 

* 1 onion 
* ½ inch ginger 
* 9 cloves Garlic
* ⅓ cup stock 
* 1 tsp sugar 
* 3 tbsp salt 
* ½ - 1 cup gochu garu (korean red chilli flakes)

Instructions
~~~~~~~~~~~~

* Quarter the napa cabbage and remove the stalks. Green is good. Chop into bite size pieces and sprinkle with the salt. The salt will be rinsed off later. Add 1 cup of water, toss and let sit for 2 hours. Wash the napa cabbage with water to clean it and to remove the salt.
* Chop the radish and carrots into thin slices. Chop the green onions diagonally into ½ cm slices and mix with the napa cabbage after rinsing. 
* Meanwhile prepare mix the rice flour with water, mix well and on medium high heat for a few minutes until it thickens and bubbles. Let it cool down. 
* Chop the onions, ginger, and garlic. Add to the rice paste with 2 tbsp salt, 1 tsp sugar and ⅓ cup vegetable broth. Add the gochugaru as needed. Puree in processor.
* Mix the kimchi paste with the chopped napa cabbage and other vegetables and pack tightly in air-tight containers.

Description
~~~~~~~~~~~

The kimchi will ferment in the refrigerator in 2 weeks. It will ferment in 1-2 days at room temperature (75F).

From `Maangchi's Vegetarian Kimchi recipe <https://www.maangchi.com/recipe/chaesik-kimchi>`_.
