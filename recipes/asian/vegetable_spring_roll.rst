Vegetable Spring Roll
---------------------

*needs review* 

Ingredients
~~~~~~~~~~~

* Rice paper 

Filling 
+++++++

* Carrot, julienned 
* Radish, julienned 
* Lettuce, chopped  
* Bean sprouts 
* Crushed nuts 
* Vermicelli 
* Tofu
* Coriander leaves, mint leaves, chives, green onions  

Peanut sauce 
++++++++++++ 

* 1/4 cup peanut butter 
* 2 tbsp hoisin sauce 
* 1 tbsp soy sauce 
* 1 tsp lime or lemon juice 
* 1 medium garlic clove grated 
* 1 tbsp sriracha (optional)


Instructions
~~~~~~~~~~~~

* Cook the vermicelli in hot water and set aside.
* Prepare the vegetable mixture.
* Submerge the rice paper in water for a few seconds and place it on a plate. Spoon some vermicelli and the vegetable mixture in the middle. Fold in the edges and roll firmly.

Description
~~~~~~~~~~~

Vegetable spring rolls are easy to make and a healthy dinner. My daughter loves to make them for her school cafe where they were popular!

