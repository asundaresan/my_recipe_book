Vegetarian Pho
--------------

Ingredients
~~~~~~~~~~~

For the Pho
+++++++++++

* 2 cinnamon sticks 
* 3 cloves 
* 2 star anise
* 1 onion, thinly sliced 
* 2 big pieces of ginger 
* Vegetable stock 
* Water 

Toppings 
++++++++

* Basil leaves 
* Mint leaves 
* Bean sprouts
* Green onions
* Jalapenos 
* Lime
* Tofu 


Instructions
~~~~~~~~~~~~

* Warm the oil in a pan and add the cinnamon, cloves and star anise and toast until fragrant. 

* Add the onion and ginger and char (?) well 

* Add the vegetable stock, water and salt and simmer for 30 minutes. 

* Add noodles, shitake mushrooms and other toppings after straining the soop. 

Description
~~~~~~~~~~~

Vegetarian Pho
