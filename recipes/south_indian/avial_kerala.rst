Kerala Avial
---------------------------

*** Needs review ***

Ingredients
~~~~~~~~~~~

Avial is a mixture of many vegetables chopped into cubes or strips. The following is a sample but you may add others as well.

* 1 cup winter gourd
* 1 Chayote squash
* 1 Podalanga (snake gourd)
* 1 Plantain 
* Juice from 2 Tbsp tamarind
* 2 tsp Turmeric 

For the curry paste 

* 1 cup coconut (fresh or frozen)
* A few thai or indian green chillies (to taste)
* A few red chillies (to taste)
* 1 tbsp Cumin seeds

For the Tadka

* 2 tsp black mustard seeds 
* A sprig of curry leaves 
* A pinch of asafoetida (hing)


Instructions
~~~~~~~~~~~~

* Grind the coconut, green and red chillies along with cumin into a coarse paste and set aside. 

* Prepare the tadka. 

* Boil the vegetables with turmeric and tamarind juice until they are cooked but firm. Add salt when done.Pour the curry paste on top of the vegetables.  

* Simmer for a few minutes and add the tadka.

Description
~~~~~~~~~~~

Avial is a mixed vegetable in coconut paste from South India and a favorite of the family. This is the Kerala version of the avial. 
