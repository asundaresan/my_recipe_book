.. _venn_pongal:

.. index::
   Pongal
   South Indian 

Venn Pongal
-----------

Ingredients
~~~~~~~~~~~

* 1 cup short-grained rice
* ½ cup moong dal
* 4½ - 5 cups water
* 1 tsp salt (to taste)

Tadka 
+++++

* 1 tbsp ghee 
* ½ cup cashew nuts (optional)
* ½ inch ginger grated 
* 1 tsp ground cumin
* a few pepper corns 
* 1 chopped green chillies (to taste)
* 10 curry leaves 

Instructions
~~~~~~~~~~~~

* Dry roast the rice and moong dal for 5-10 minutes until fragrant. Wash the rice and dal and add water. Cook in instant pot high pressure for 15 minutes. 

* Meanwhile, prepare the seasoning. Heat the ghee in a pan and add the cashews, curry leaves and other spices. Saute the cashews on low-flame, stirring constantly for a few minutes, until the cashews are toasted. 

* Add the tadka to the pongal and serve hot with coconut chutney. 

Description
~~~~~~~~~~~

Serve hot with coconut chutney. 

