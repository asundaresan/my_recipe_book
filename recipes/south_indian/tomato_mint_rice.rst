Tomato mint rice
----------------

Ingredients
~~~~~~~~~~~

* 1 cup Basmati rice 
* 2 cups water 
* Onions 
* Tomatoes 
* Mint leaves
* Ginger-garlic paste



Instructions
~~~~~~~~~~~~

* Saute onions. When brown add mint leaves, tomatoes, ginger-garlic paste and saute some more. 
* Grind into a paste and saute in ghee. 
* When the oil separates, add rice, salt and water. Cook for 25 minutes at low heat in Dutch oven. Mix once after 12 minutes.  

Description
~~~~~~~~~~~

Adapted from Malathi Sundaresan's recipe.  

