.. _sauerkraut: 

Sauerkraut
----------

Ingredients
~~~~~~~~~~~

* Green Cabbage
* Sea salt (without iodine)
* Garlic, grated 
* Ginger, grated
* Turmeric root, grated
* Jalapenos or habaneros, chopped fine


Instructions
~~~~~~~~~~~~

* Slice cabbage into thin pieces (save a cabbage leaf for covering it).

* Add rest of ingredients and mix in large bowl. You can add different herbs and flavors such as garlic, ginger, turmeric, habanero, jalapenos, etc. 

* Add sea salt (no iodine) 3 tablespoons / 5lb of cabbage and let sit for 5 minutes .

* Massage the cabbage for 5-10 minutes with hand - the water in the cabbage will be extracted by the salt and massaging. 

* Press the mixture firmly into a jar - the cabbage should be submerged in 1-2cm of the water.

* Cut out a circular piece of cabbage and use it to push the floating pieces under the brine and add a weight like a stone.

* Store in a warm place for 1-2 weeks, occasionally stirring the top and removing pieces floating to the top. In 2-3 days, there should be bubbles forming. While the cabbage can be fermented for upto 4 weeks, our family prefers mildly fermented sauerkraut for 8-10 days. 


Description
~~~~~~~~~~~

The garlic, ginger, turmeric root, jalapenos, etc are all optional but they provide a great flavor!

