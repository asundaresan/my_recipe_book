.. _fermented_tea_leaves:

Fermented tea leaves 
--------------------

.. index::
   fermentation

Ingredients
~~~~~~~~~~~

For roughly 8 servings. 

* 12 used tea bags (Sencha tea)
* 6 cloves garlic, coarsely chopped and mashed 
* 2 tablespoons distilled white vinegar (or kombucha)
* 1-2 tsp sea salt
* 3 tablespoons canola oil (or fried garlic oil)


Instructions
~~~~~~~~~~~~

The tea should be steeped in water for around 5-10 minutes. I use the tea for kombucha and the tea leaves for this recipe. 
Mix the tea leaves, garlic and salt in a mini food processor (I use a nutri-bullet).  Add in the oil and vinegar. Once the ingredients are mixed well, transfer to a bottle and keep in a dark, warm place (70-75F) for 1-2 weeks. Keep checking every couple of days to make sure it is not spoiling. 

Description
~~~~~~~~~~~

Since we started making kombucha at home, I have also started occasionally making fermented tea leaves (or Laphet) as a by product - why throw away the tea leaves? This can be used to make :ref:`burmese_tea_leaf_salad`. This is based on the recipes posted at `Home made Lahpet <https://whattocooktoday.com/homemade-lahpet-burmese-pickled-tea-leaves.html>`_ and `Tea leaf salad <https://www.eatingwell.com/recipe/262787/tea-leaf-salad/>`_.
