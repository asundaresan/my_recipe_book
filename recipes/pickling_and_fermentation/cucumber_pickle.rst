.. _cucumber_pickle:

Cucumber pickle
---------------


Ingredients
~~~~~~~~~~~

Brine 
+++++
* 4 cups water
* 1 cup distilled white vinegar
* 2 tablespoons sea salt

Pickle and flavoring 
++++++++++++++++++++
* 8 Cucumbers
* Black pepper
* Bay leaves
* Whole coriander seeds 
* Whole mustard seeds 
* Dried red chillies
* Sliced onions (optional)
* Dill

Instructions
~~~~~~~~~~~~

* Bring water to a boil and add the vinegar and the salt. Let it cool. Add the spices to let it soak in.

* Slice cucumbers into rounds or length wise. Soak in ice to keep it crisp.

* Pack the cucumbers and other vegetables along with the herbs into glass jars. Pour the brine in so that the vegetables are submerged. Refrigerate. 

* They should be ready to use in 6 hours or so and keep in the refrigerator for weeks.

Description
~~~~~~~~~~~

You may also pickle carrots, radishes the same way. 
