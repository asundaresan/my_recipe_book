.. _kombucha: 

Kombucha
--------

.. index:: 
   fermentation
   beverages

Ingredients
~~~~~~~~~~~

Makes 1 gallon of Kombucha.

First fermentation 
++++++++++++++++++

* 6 tea bags (Sencha)
* 1 cup sugar 
* 1 SCOBY (Symbiotic Culture Of Bacteria and Yeast)
* 1 cup starter Kombucha

Second fermentation 
+++++++++++++++++++

* 4-5 cups of fruit juice 


Instructions
~~~~~~~~~~~~

First fermentation 
++++++++++++++++++

* Bring roughly 8 cups of water to a boil and turn off the heat. Steep the tea bags for 5-10 minutes. You may discard the tea bags or use them to make :ref:`fermented_tea_leaves`.

* Dissolve the sugar in the water and pour into a glass gallon jar. Top up with filtered water to bring it approximately 7/8ths full. 

* Once the solution is at room temperature, add the starter kombucha and the SCOBY. The jar should now be at the 1 gallon level. 

* Cover with a coffe filter and rubber band and place in a dark place around 75F for 1-2 weeks (depending on the temperature). If it is slightly warmer, then 1 week should suffice but if it is slightly cooler, than ferment for 2 weeks. Keep tasting after a week to check if the Kombucha flavor is to your liking. 


Second fermentation 
+++++++++++++++++++ 

* Once the first fermentation is over, set aside 1 cup of Kombucha and the SCOBY for the next batch. The kombucha is ready to drink as-is. You may also add flavor and bottle the kombucha to get some fizz. 

* Bottle the kombucha with fresh fruit or fruit juice. Pour 1/2 cup to 3/4 cup juice into a flip-top bottle and top up with kombucha. Leave 2-3 cm of space near the top to allow for pressure to build-up. 

* Leave the second fermentation for 2-3 days depending on your tastes and refrigerate after that!

Description
~~~~~~~~~~~

Kombucha making is fun and reasonably forgiving in terms of the days. If you ferment it longer, it does get more sour and more acidic. You have to find the right balance that works for you!

