Asian 
***** 

.. toctree::
   :maxdepth: 1
   :glob: 
   
   asian/*

Beverages
*********

.. toctree::
   :maxdepth: 1
   :glob: 
   
   beverages/*


Cakes and breads
**************** 

.. toctree::
   :maxdepth: 1
   :glob: 
   
   cakes_and_breads/*


Indian 
******

.. toctree::
   :maxdepth: 1
   :glob: 
   
   indian/*


South Indian 
************ 

.. toctree::
   :maxdepth: 1
   :glob: 
   
   south_indian/*


Salads
******

.. toctree::
   :maxdepth: 1
   :glob: 
   
   salads/*


World
*****

.. toctree::
   :maxdepth: 1
   :glob: 
   
   world/*


Pickling and fermentation
*************************

.. toctree::
   :maxdepth: 1
   :glob: 
   
   pickling_and_fermentation/*


