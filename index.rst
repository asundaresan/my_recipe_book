.. My recipe book! documentation master file, created by
   sphinx-quickstart on Wed Feb  7 20:20:51 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

My recipe book!
===============


.. toctree::
   :maxdepth: 1
   :glob:
   
   recipes/all




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
